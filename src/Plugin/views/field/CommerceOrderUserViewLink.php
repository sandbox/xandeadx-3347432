<?php

namespace Drupal\commerce_improvements\Plugin\views\field;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\EntityLink;
use Drupal\views\ResultRow;

/**
 * @ViewsField("commerce_order_user_view_link")
 */
class CommerceOrderUserViewLink extends EntityLink {

  /**
   * {@inheritDoc}
   */
  protected function getEntityLinkTemplate(): string {
    return 'user-view';
  }

  /**
   * {@inheritDoc}
   */
  protected function getUrlInfo(ResultRow $row): Url {
    $url = parent::getUrlInfo($row);
    $order = $this->getEntity($row); /** @var OrderInterface $order */
    $url->setRouteParameter('user', $order->getCustomerId());
    return $url;
  }

}
