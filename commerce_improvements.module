<?php

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_entity_base_field_info_alter().
 *
 * @param BaseFieldDefinition[] $fields
 */
function commerce_improvements_entity_base_field_info_alter(array &$fields, EntityTypeInterface $entity_type): void {
  if ($entity_type->id() == 'commerce_product') {
    /** @see \Drupal\commerce_product\Entity\Product::baseFieldDefinitions() */
    $fields['created']->setLabel(t('Date of creation'));
    $fields['changed']->setLabel(t('Date of change'));
  }
  elseif ($entity_type->id() == 'commerce_order') {
    /** @see \Drupal\commerce_order\Entity\Order::baseFieldDefinitions() */
    $fields['placed']->setLabel(t('Date of placement'));
    $fields['changed']->setLabel(t('Date of change'));
    $fields['completed']->setLabel(t('Date of completion'));
  }
}

/**
 * Implements hook_views_data().
 */
function commerce_improvements_views_data(): array {
  $data['commerce_order']['user_view_commerce_order'] = [
    'field' => [
      'title' => t('Link to @entity_type_label', ['@entity_type_label' => t('Order')]) . ' ' . t('on behalf of user'),
      'help' => t('Link to @entity_type_label', ['@entity_type_label' => t('Order')]) . ' ' . t('on behalf of user'),
      'id' => 'commerce_order_user_view_link',
    ],
  ];

  return $data;
}

/**
 * Implements hook_entity_type_alter().
 *
 * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
 */
function commerce_improvements_entity_type_alter(array &$entity_types): void {
  if (isset($entity_types['commerce_order']) && !$entity_types['commerce_order']->hasLinkTemplate('user-view')) {
    $entity_types['commerce_order']->setLinkTemplate('user-view', '/user/{user}/orders/{commerce_order}');
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter(): commerce_checkout_form.
 */
function commerce_improvements_theme_suggestions_commerce_checkout_form_alter(array &$suggestions, array $variables): void {
  $suggestions[] = $variables['theme_hook_original'] . '__' . $variables['form']['#step_id'];
}
